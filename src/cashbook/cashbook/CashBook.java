/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cashbook;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author Mikhail
 */
public class CashBook extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainWindow.fxml"));
        Parent root = loader.load();
        
        MainWindowController controller = (MainWindowController) loader.getController();
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        
        stage.setOnHiding((WindowEvent event) -> {
            controller.getAdapter().close();
            try {
                controller.getConnection().close();
                controller.getJfr().close();
            } catch (IOException | SQLException ex) {
                Utils.showErrorBox("Ошибка закрытия приложения", ex);
            }
        });
        
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        DaoFactory.get().close();
        super.stop();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Updater updater = new Updater();
            updater.doUpdates();
            updater.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        launch(args);
    }
    
}
