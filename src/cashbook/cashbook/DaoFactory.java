package cashbook;

import cashbook.model.SmrModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by misha on 02.01.2017.
 */
public class DaoFactory {
    private static DaoFactory factory;
    private ConnectionSource connectionSource;

    private Dao<SmrModel, Integer> smrDao;

    private DaoFactory() throws SQLException {
        connectionSource = new JdbcConnectionSource("jdbc:sqlite:cash.db");
    }

    public static DaoFactory get() throws SQLException {
        if(factory == null) factory = new DaoFactory();
        return factory;
    }

    public Dao<SmrModel, Integer> getSmrDAO() throws SQLException {
        if(smrDao == null) smrDao = DaoManager.createDao(connectionSource, SmrModel.class);
        return smrDao;
    }

    public void close() throws IOException {
        connectionSource.close();
    }
}
