package cashbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by misha on 02.01.2017.
 */
public class PropertiesManager {
    private static PropertiesManager propertiesManager;
    private Properties properties;

    private PropertiesManager() {
        properties = new Properties();
        try {
            InputStream inputStream = new FileInputStream("config.properties");
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static PropertiesManager get() {
        if(propertiesManager == null) propertiesManager = new PropertiesManager();
        return propertiesManager;
    }

    public Properties getProperties() {
        return properties;
    }

    public void save() {
        try {
            properties.store(new FileOutputStream("config.properties"), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
