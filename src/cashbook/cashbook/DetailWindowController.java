/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cashbook;

import cashbook.model.DetailTableModel;
import cashbook.model.MainTableModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import ru.inversionkavkaz.javafxdbutils.tableview.ModelTableViewAdapter;
import ru.inversionkavkaz.javafxdbutils.tableview.model.BaseModel;
import ru.inversionkavkaz.javafxdbutils.tableview.model.WrongModelFieldTypeException;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author Mikhail
 */
public class DetailWindowController implements Initializable {
    
    private MainTableModel mainRow;
    private Connection connection;
    private ModelTableViewAdapter<MainTableModel> mainAdapter;
    private ModelTableViewAdapter<DetailTableModel> adapter;

    @FXML
    private DatePicker dateEdt;

    @FXML
    private TextField inRestEdt;
    
    @FXML
    private Button addBtn;

    @FXML
    private Button delBtn;

    @FXML
    private TableView<DetailTableModel> detailTable;

    @FXML
    private TableColumn<DetailTableModel, String> docNumCol;

    @FXML
    private TableColumn<DetailTableModel, String> docInfoCol;

    @FXML
    private TableColumn<DetailTableModel, String> accNumCol;

    @FXML
    private TableColumn<DetailTableModel, Double> prihodCol;

    @FXML
    private TableColumn<DetailTableModel, Double> rashodCol;

    @FXML
    private TableColumn<DetailTableModel, String> purpCol;

    @FXML
    private TextField outRestEdt;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;
    
    @FXML
    private Spinner<Integer> listSpinner;

    @FXML
    private TextField prihodColEdt;

    @FXML
    private TextField rashodColEdt;

    public MainTableModel getMainRow() {
        return mainRow;
    }

    public void setMainRow(MainTableModel mainRow) {
        this.mainRow = mainRow;
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        if(mainRow.cash_date.getValue() != null && !mainRow.cash_date.getValue().isEmpty()) {
            dateEdt.setValue(LocalDate.parse(mainRow.cash_date.getValue(), fmt));
        }
        else {
            dateEdt.setValue(LocalDate.now());
            mainRow.cash_date.setValue(dateEdt.getValue().format(fmt));
        }
        dateEdt.valueProperty().addListener((ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) -> {
            mainRow.cash_date.setValue(newValue.format(fmt));
            for(DetailTableModel r : detailTable.getItems()) {
                r.cash_date.setValue(newValue.format(fmt));
            }
        });
        if(mainRow.in_rest.getValue() != null) inRestEdt.setText(mainRow.in_rest.getValue().toString());
        if(mainRow.out_rest.getValue() != null) outRestEdt.setText(mainRow.out_rest.getValue().toString());
        if(mainRow.prihod_col.getValue() != null) prihodColEdt.setText(mainRow.prihod_col.getValue());
        prihodColEdt.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            mainRow.prihod_col.setValue(newValue);
        });
        if(mainRow.rashod_col.getValue() != null) rashodColEdt.setText(mainRow.rashod_col.getValue());
        rashodColEdt.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            mainRow.rashod_col.setValue(newValue);
        });
        if(mainRow.list_num.getValue() != null) listSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 99999, mainRow.list_num.getValue()));
        else listSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 99999));
        listSpinner.valueProperty().addListener((ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) -> {
            mainRow.list_num.setValue(newValue);
        });
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public ModelTableViewAdapter<MainTableModel> getMainAdapter() {
        return mainAdapter;
    }

    public void setMainAdapter(ModelTableViewAdapter<MainTableModel> mainAdapter) {
        this.mainAdapter = mainAdapter;
    }
    
    
    @FXML
    void handleButtonAction(ActionEvent event) {
        Button source = (Button) event.getSource();
        if(source.equals(okBtn)) {
            try {
                mainAdapter.post();
                adapter.post();
                ((Stage) detailTable.getScene().getWindow()).close();
            } catch (SQLException ex) {
                Utils.showErrorBox("Ошибка сохранения данных в БД", ex);
            }
        }
        else if(source.equals(cancelBtn)) {
            if(mainRow.getState() == BaseModel.RowState.Inserted) mainAdapter.deleteCurrentRow();
            ((Stage) detailTable.getScene().getWindow()).close();
        }
        else if(source.equals(addBtn)) {
            DetailTableModel r = adapter.addRow();
            DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            r.cash_date.setValue(dateEdt.getValue().format(fmt));
        }
        else if(source.equals(delBtn)) {
            if(detailTable.getSelectionModel().getSelectedItem() != null)
                adapter.deleteCurrentRow();
        }
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inRestEdt.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            mainRow.in_rest.setValue(Double.parseDouble(newValue));
            double out_rest = mainRow.in_rest.getValue();
            for(DetailTableModel r : detailTable.getItems()) {
                if(r.prihod.getValue() != null) out_rest += r.prihod.getValue();
                if(r.rashod.getValue() != null) out_rest -= r.rashod.getValue();
            }
            outRestEdt.setText(((Double)out_rest).toString());
        });
        outRestEdt.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            mainRow.out_rest.setValue(Double.parseDouble(newValue));
        });
        detailTable.editingCellProperty().addListener(new ChangeListener<TablePosition<DetailTableModel, ?>>() {
            @Override
            public void changed(ObservableValue<? extends TablePosition<DetailTableModel, ?>> observable, TablePosition<DetailTableModel, ?> oldValue, TablePosition<DetailTableModel, ?> newValue) {
                double out_rest = mainRow.in_rest.getValue();
                for(DetailTableModel r : detailTable.getItems()) {
                    if(r.prihod.getValue() != null) out_rest += r.prihod.getValue();
                    if(r.rashod.getValue() != null) out_rest -= r.rashod.getValue();
                }
                outRestEdt.setText(((Double)out_rest).toString());
            }
        });
    }

    public void createAdapter() {
        try {
            DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            adapter = new ModelTableViewAdapter<>(DetailTableModel.class, detailTable, connection, "select * from detail_table where cash_date = ?", new Object[] { dateEdt.getValue().format(fmt) });
            adapter.addColumn("doc_num", docNumCol);
            adapter.addColumn("doc_info", docInfoCol);
            adapter.addColumn("acc_num", accNumCol);
            adapter.addColumn("prihod", prihodCol);
            adapter.addColumn("rashod", rashodCol);
            adapter.addColumn("purp", purpCol);
            adapter.setEditable(true);
            adapter.openQuery();
            
            
            if(mainRow.getState() == BaseModel.RowState.Inserted) {
                
                String lastDt = "";
                
                DateTimeFormatter fmt2 = DateTimeFormatter.ofPattern("yyyyMMdd");
                PreparedStatement st = connection.prepareStatement("select max(substr(cash_date, 7) || substr(cash_date, 4, 2) || substr(cash_date, 1, 2)) from main_table where substr(cash_date, 7) || substr(cash_date, 4, 2) || substr(cash_date, 1, 2) < ?");
                st.setString(1, dateEdt.getValue().format(fmt2));
                ResultSet rs = st.executeQuery();
                if(rs.next()) {
                    lastDt = rs.getString(1);
                }
                rs.close();
                st.close();

                st = connection.prepareStatement("select a.doc_num, a.doc_info, a.acc_num, a.prihod, a.rashod, a.purp from detail_table a where substr(a.cash_date, 7) || substr(a.cash_date, 4, 2) || substr(a.cash_date, 1, 2) = ?");
                st.setString(1, lastDt);
                rs = st.executeQuery();
                while(rs.next()) {
                    DetailTableModel r = adapter.addRow();
                    r.cash_date.setValue(dateEdt.getValue().format(fmt));
                    r.doc_num.setValue(rs.getString("doc_num"));
                    r.doc_info.setValue(rs.getString("doc_info"));
                    r.acc_num.setValue(rs.getString("acc_num"));
                    r.prihod.setValue(rs.getDouble("prihod"));
                    r.rashod.setValue(rs.getDouble("rashod"));
                    r.purp.setValue(rs.getString("purp"));
                }
                rs.close();
                st.close();
                
                st = connection.prepareStatement("select out_rest, prihod_col, rashod_col from main_table where substr(cash_date, 7) || substr(cash_date, 4, 2) || substr(cash_date, 1, 2) = ? order by cash_date desc");
                st.setString(1, lastDt);
                rs = st.executeQuery();
                if(rs.next()) {
                    inRestEdt.setText(((Double)rs.getDouble(1)).toString());
                    prihodColEdt.setText(rs.getString(2));
                    rashodColEdt.setText(rs.getString(3));
                }
                rs.close();
                st.close();
            }
            
        } catch (WrongModelFieldTypeException | SQLException ex) {
            Utils.showErrorBox("Ошибка получения данных из БД", ex);
        }
    }
    
    public void closeAdapter() {
        adapter.close();
    }
    
}
