/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cashbook;

import cashbook.model.MainTableModel;
import cashbook.model.SmrModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import ru.inversionkavkaz.javafxdbutils.tableview.ModelTableViewAdapter;
import ru.inversionkavkaz.javafxdbutils.tableview.model.WrongModelFieldTypeException;
import ru.inversionkavkaz.jfr.JFRRunner;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 *
 * @author Mikhail
 */
public class MainWindowController implements Initializable {
    
    private Connection connection;
    
    private JFRRunner jfr;
    
    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;
    
    @FXML
    private Button printBtn;

    @FXML
    private Button designBtn;

    @FXML
    private TableView<MainTableModel> mainTable;

    @FXML
    private TableColumn<MainTableModel, Date> dateCol;

    @FXML
    private TableColumn<MainTableModel, Double> inRestCol;

    @FXML
    private TableColumn<MainTableModel, Double> outRestCol;
    
    private ModelTableViewAdapter<MainTableModel> adapter;

    @FXML
    private ComboBox<SmrModel> smrCB;

    @FXML
    private Button addSrmBtn;

    @FXML
    private Button editSmrBtn;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        Button source = (Button)event.getSource();
        if(source.equals(addBtn)) {
            addNew();
        }
        else if(source.equals(delBtn)) {
            del();
        }
        else if(source.equals(editBtn)) {
            edit();
        }
        else if(source.equals(printBtn)) {
            MainTableModel r = mainTable.getSelectionModel().getSelectedItem();
            if(r != null) {
                try {
                    jfr.setParam("pdt", r.cash_date.getValue());
                    jfr.run("report.fr3");
                } catch (IOException ex) {
                    Utils.showErrorBox("Ошибка печати отчета", ex);
                }
            }
        }
        else if(source.equals(designBtn)) {
            MainTableModel r = mainTable.getSelectionModel().getSelectedItem();
            if(r != null) {
                try {
                    jfr.setParam("pdt", r.cash_date.getValue());
                    jfr.design("report.fr3");
                } catch (IOException ex) {
                    Utils.showErrorBox("Ошибка редактирования отчета", ex);
                }
            }
        }
        else if(source.equals(addSrmBtn)) {
            addSmr();
        }
        else if(source.equals(editSmrBtn)) {
            SmrModel selSmr = smrCB.getSelectionModel().getSelectedItem();
            if(selSmr != null) {
                editSmr(selSmr.getId());
            } else {
                Utils.showInfoBox("Редактирование организации", "Выберите организацию из списка", "Тогда вы сможете отредактировать ее");
            }
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public ModelTableViewAdapter<MainTableModel> getAdapter() {
        return adapter;
    }

    public JFRRunner getJfr() {
        return jfr;
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            DriverManager.registerDriver(new org.sqlite.JDBC());
            connection = DriverManager.getConnection("jdbc:sqlite:cash.db");
            jfr = new JFRRunner(JFRRunner.Providers.SQLite, "cash.db", null, null, null);
            adapter = new ModelTableViewAdapter<>(MainTableModel.class, mainTable, connection, "select * from main_table", null);
            adapter.addColumn("cash_date", dateCol);
            adapter.addColumn("in_rest", inRestCol);
            adapter.addColumn("out_rest", outRestCol);
            adapter.openQuery();
        } catch (SQLException | WrongModelFieldTypeException ex) {
            Utils.showErrorBox("Ошибка соединения с БД", ex);
        }

        loadSMR();
        String smrIdStr = PropertiesManager.get().getProperties().getProperty("smr_id");
        if(smrIdStr != null && !smrIdStr.isEmpty()) {
            int smr_id = Integer.valueOf(smrIdStr);
            List<SmrModel> models = smrCB.getItems();
            Optional<SmrModel> opt = models.stream().filter((SmrModel s) -> { return s.getId() == smr_id; }).findFirst();
            if(opt.isPresent()) {
                smrCB.getSelectionModel().select(opt.get());
            }
        }
        smrCB.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<SmrModel>() {
            @Override
            public void changed(ObservableValue<? extends SmrModel> observable, SmrModel oldValue, SmrModel newValue) {
                PropertiesManager.get().getProperties().setProperty("smr_id", String.valueOf(newValue.getId()));
                PropertiesManager.get().save();
            }
        });
    }

    private void addNew() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("DetailWindow.fxml"));
        try {
            Parent root = loader.load();
            DetailWindowController controller = (DetailWindowController) loader.getController();
            controller.setConnection(connection);
            MainTableModel r = adapter.addRow();
            controller.setMainRow(r);
            controller.setMainAdapter(adapter);
            controller.createAdapter();
            Stage stage = new Stage();// mainVbox.getScene().getWindow();
            stage.setOnHidden((WindowEvent event) -> {
                controller.closeAdapter();
            });
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner((Stage) mainTable.getScene().getWindow());
            stage.setTitle("Содержимое даты");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.showAndWait();
        } catch (IOException ex) {
            Utils.showErrorBox("Ошибка открытия окна содержимого даты", ex);
        }
    }
    
    private void del() {
        if(mainTable.getSelectionModel().getSelectedItem() == null) return;
        ButtonType bt = Utils.showConfirmationBox("Удаление даны", "Вы действительно хотите удалить дату?", null, Utils.DialogButtons.YES_NO);
        if(bt.equals(ButtonType.YES)) {
            try {
                PreparedStatement st = connection.prepareStatement("delete from detail_table where cash_date = ?");
                st.setString(1, mainTable.getSelectionModel().getSelectedItem().cash_date.getValue());
                st.executeUpdate();
                adapter.deleteCurrentRow();
                adapter.post();
            } catch (SQLException ex) {
                Utils.showErrorBox("Ошибка удаления даты", ex);
            }
        }
    }
    
    private void edit() {
        if(mainTable.getSelectionModel().getSelectedItem() == null) return;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("DetailWindow.fxml"));
        try {
            Parent root = loader.load();
            DetailWindowController controller = (DetailWindowController) loader.getController();
            controller.setConnection(connection);
            MainTableModel r = mainTable.getSelectionModel().getSelectedItem();
            controller.setMainRow(r);
            controller.setMainAdapter(adapter);
            controller.createAdapter();
            Stage stage = new Stage();// mainVbox.getScene().getWindow();
            stage.setOnHidden((WindowEvent event) -> {
                controller.closeAdapter();
            });
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner((Stage) mainTable.getScene().getWindow());
            stage.setTitle("Содержимое даты");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.showAndWait();
        } catch (IOException ex) {
            Utils.showErrorBox("Ошибка открытия окна содержимого даты", ex);
        }
    }

    private void loadSMR() {
        try {
            List<SmrModel> smrModels = DaoFactory.get().getSmrDAO().queryForAll();
            smrCB.setItems(FXCollections.observableArrayList(smrModels));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addSmr() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("SmrWindow.fxml"));
        try {
            Parent root = loader.load();
            SmrController smrController = loader.getController();
            smrController.setAddNewRecord(true);
            smrController.init();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner((Stage) mainTable.getScene().getWindow());
            stage.setTitle("Редактирование организации");
            stage.setScene(new Scene(root));
            stage.setOnHidden(event -> loadSMR());
            stage.showAndWait();
        } catch (IOException e) {
            Utils.showErrorBox("Ошибка открытия окна редактирования организации", e);
        }
    }

    private void editSmr(int smrId) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("SmrWindow.fxml"));
        try {
            Parent root = loader.load();
            SmrController smrController = loader.getController();
            smrController.setAddNewRecord(false);
            smrController.setSmrId(smrId);
            smrController.init();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner((Stage) mainTable.getScene().getWindow());
            stage.setTitle("Редактирование организации");
            stage.setScene(new Scene(root));
            stage.setOnHidden(event -> loadSMR());
            stage.showAndWait();
        } catch (IOException e) {
            Utils.showErrorBox("Ошибка открытия окна редактирования организации", e);
        }
    }
}
