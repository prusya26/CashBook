package cashbook;

import cashbook.model.SmrModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Created by misha on 03.01.2017.
 */
public class SmrController implements Initializable {

    private boolean addNewRecord;
    private int smrId;
    private SmrModel smrModel;

    @FXML
    private TextField idEdt;

    @FXML
    private TextField nameEdt;

    @FXML
    private TextField podrazdEdt;

    @FXML
    private TextField kassirEdt;

    @FXML
    private TextField buhEdt;

    @FXML
    private Button saveBtn;

    @FXML
    private Button cancelBtn;

    @FXML
    void handleButtonAction(ActionEvent event) {
        if(event.getSource().equals(saveBtn)) {
            try {
                DaoFactory.get().getSmrDAO().createOrUpdate(smrModel);
                ((Stage) saveBtn.getScene().getWindow()).close();
            } catch (SQLException e) {
                Utils.showErrorBox("Ошибка сохранение организации", e);
            }
        } else if(event.getSource().equals(cancelBtn)) {
            ((Stage) saveBtn.getScene().getWindow()).close();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public boolean isAddNewRecord() {
        return addNewRecord;
    }

    public void setAddNewRecord(boolean addNewRecord) {
        this.addNewRecord = addNewRecord;
    }

    public int getSmrId() {
        return smrId;
    }

    public void setSmrId(int smrId) {
        this.smrId = smrId;
    }

    public void init() {
        if(!isAddNewRecord()) {
            try {
                smrModel = DaoFactory.get().getSmrDAO().queryForId(smrId);
                if(smrModel == null) {
                    smrModel = new SmrModel();
                }
            } catch (SQLException e) {
                smrModel = new SmrModel();
            }
        } else {
            smrModel = new SmrModel();
        }
        idEdt.setText(String.valueOf(smrModel.getId()));
        nameEdt.setText(smrModel.getName());
        podrazdEdt.setText(smrModel.getPodrazd());
        kassirEdt.setText(smrModel.getKassir());
        buhEdt.setText(smrModel.getBuh());

        nameEdt.textProperty().addListener((observable, oldValue, newValue) -> smrModel.setName(newValue));

        podrazdEdt.textProperty().addListener((observable, oldValue, newValue) -> smrModel.setPodrazd(newValue));

        kassirEdt.textProperty().addListener((observable, oldValue, newValue) -> smrModel.setKassir(newValue));

        buhEdt.textProperty().addListener((observable, oldValue, newValue) -> smrModel.setBuh(newValue));
    }
}
