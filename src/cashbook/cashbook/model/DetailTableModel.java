/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cashbook.model;

import ru.inversionkavkaz.javafxdbutils.tableview.model.*;

/**
 *
 * @author Mikhail
 */
@TableInfo(name = "detail_table")
public class DetailTableModel extends BaseModel {
    @PK
    @Column
    public StringModelField cash_date;
    @PK
    @Column
    public StringModelField doc_num;
    @Column
    public StringModelField doc_info;
    @Column
    public StringModelField acc_num;
    @Column
    public DoubleModelField prihod;
    @Column
    public DoubleModelField rashod;
    @Column
    public StringModelField purp;
}
