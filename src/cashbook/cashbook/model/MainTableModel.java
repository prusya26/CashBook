/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cashbook.model;

import ru.inversionkavkaz.javafxdbutils.tableview.model.BaseModel;
import ru.inversionkavkaz.javafxdbutils.tableview.model.Column;
import ru.inversionkavkaz.javafxdbutils.tableview.model.DoubleModelField;
import ru.inversionkavkaz.javafxdbutils.tableview.model.IntegerModelField;
import ru.inversionkavkaz.javafxdbutils.tableview.model.PK;
import ru.inversionkavkaz.javafxdbutils.tableview.model.StringModelField;
import ru.inversionkavkaz.javafxdbutils.tableview.model.TableInfo;

/**
 *
 * @author Mikhail
 */
@TableInfo(name = "main_table")
public class MainTableModel extends BaseModel {
    @PK
    @Column
    public StringModelField cash_date;
    @Column
    public DoubleModelField in_rest;
    @Column
    public DoubleModelField out_rest;
    @Column
    public IntegerModelField list_num;
    @Column
    public StringModelField prihod_col;
    @Column
    public StringModelField rashod_col;
}
