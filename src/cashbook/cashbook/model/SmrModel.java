package cashbook.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by misha on 02.01.2017.
 */
@DatabaseTable(tableName = "smr")
public class SmrModel {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String podrazd;
    @DatabaseField
    private String kassir;
    @DatabaseField
    private String buh;

    public SmrModel() {
    }

    public SmrModel(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPodrazd() {
        return podrazd;
    }

    public void setPodrazd(String podrazd) {
        this.podrazd = podrazd;
    }

    public String getKassir() {
        return kassir;
    }

    public void setKassir(String kassir) {
        this.kassir = kassir;
    }

    public String getBuh() {
        return buh;
    }

    public void setBuh(String buh) {
        this.buh = buh;
    }

    @Override
    public String toString() {
        return name;
    }
}
