/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cashbook;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 *
 * @author Mikhail
 */
public class Utils {
    public static void showErrorBox(String erorMsg, Exception ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка");
        alert.setHeaderText(erorMsg);
        alert.setContentText(ex.getLocalizedMessage());
        
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();
        
        Label label = new Label("Stack Trace возникшей ошибки (Передайте эту информацию разработчику):");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }
    
    public static void showInfoBox(String title, String headerText, String contentText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }
    
    public static void showWarningBox(String title, String headerText, String contentText) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }
    
    public enum DialogButtons {
        YES_NO,
        YES_NO_CANCEL,
        OK_CANCEL
    }
    
    public static ButtonType showConfirmationBox(String title, String headerText, String contentText, DialogButtons db) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        ButtonType yes = new ButtonType("Да", ButtonBar.ButtonData.YES);
        ButtonType no = new ButtonType("Нет", ButtonBar.ButtonData.NO);
        ButtonType ok = new ButtonType("ОК", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE);
        switch(db) {
            case YES_NO: alert.getButtonTypes().setAll(yes, no); break;
            case YES_NO_CANCEL: alert.getButtonTypes().setAll(yes, no, cancel);
            case OK_CANCEL: alert.getButtonTypes().setAll(ok, cancel);            
        }

        Optional<ButtonType> result = alert.showAndWait();
        if(result.get().equals(yes)) return ButtonType.YES;
        else if(result.get().equals(no)) return ButtonType.NO;
        else if(result.get().equals(ok)) return ButtonType.OK;
        else return ButtonType.CANCEL;
    }
}
