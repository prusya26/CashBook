package cashbook;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by misha on 02.01.2017.
 */
public class Updater {
    private int currentVersion;
    private Connection connection;
    private ScriptRunner scriptRunner;

    public Updater() throws SQLException {
        currentVersion = Integer.valueOf(PropertiesManager.get().getProperties().getProperty("db_ver", "1"));
        DriverManager.registerDriver(new org.sqlite.JDBC());
        connection = DriverManager.getConnection("jdbc:sqlite:cash.db");
        scriptRunner = new ScriptRunner(connection, true, false);
        scriptRunner.setDelimiter(";", false);
    }

    public void doUpdates() {
        File updPath = new File("updates");
        if(updPath.exists()) {
            File[] files = updPath.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    Pattern pattern = Pattern.compile("v\\d+\\.upd");
                    Matcher matcher = pattern.matcher(pathname.getName());
                    boolean matches = matcher.matches();
                    if(matches) {
                        int ver = Integer.valueOf(pathname.getName().substring(1, pathname.getName().indexOf(".")));
                        return ver > currentVersion;
                    } else {
                        return false;
                    }
                }
            });

            List<File> fileList = Arrays.asList(files);
            Collections.sort(fileList, (o1, o2) -> {
                int ver1 = Integer.valueOf(o1.getName().substring(1, o1.getName().indexOf(".")));
                int ver2 = Integer.valueOf(o2.getName().substring(1, o2.getName().indexOf(".")));
                return Integer.compare(ver1, ver2);
            });

            for(File updFile : fileList) {
                try {
                    BufferedReader reader = new BufferedReader(new FileReader(updFile));
                    scriptRunner.runScript(reader);
                    PropertiesManager.get().getProperties().setProperty("db_ver", updFile.getName().substring(1, updFile.getName().indexOf(".")));
                } catch (IOException | SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void close() throws SQLException {
        connection.close();
        PropertiesManager.get().save();
    }
}
